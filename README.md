
# Bigfoot Facts

Bigfoot (also known as Sasquatch) is a cryptid in American folklore, supposedly a simian-like creature that inhabits forests, especially those of the Pacific Northwest. Bigfoot is usually described as a large, hairy, bipedal humanoid. The term sasquatch is an Anglicized derivative of the Halkomelem word sásq'ets.

# How to use
1. configure aws cli
2. clone this repo
3. npm install
4. npm deploy
5. Edit ./src/fact.sh and add your serverless http endpoint url
5. `./src/fact.sh`
