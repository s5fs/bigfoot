# is bigfoot real? `curl -sSL http://bit.ly/2mrpsAr | sh` to find out!
ENDPOINT=https://gi3s41qg2e.execute-api.us-east-1.amazonaws.com/dev/fact

PLATFORM=`uname`
FACT=`curl -s ${ENDPOINT}`
FACTID=`echo ${FACT} | sed -e 's/.*#\(.*\):.*/\1/'`
FACTFILE=https://s3-us-west-2.amazonaws.com/bigfoot-is-real/mp3/fact_${FACTID}.mp3

if [ $PLATFORM = "Darwin" ]
then
  curl -o .fact.mp3 ${FACTFILE} > /dev/null 2>&1
fi

echo
echo '----------Oooo-----'
echo '---------(----)----  ______ _        __            _'
echo '----------)--/-----  | ___ (_)      / _|          | |'
echo '----------(_/------  | |_/ /_  __ _| |_ ___   ___ | |_'
echo '----oooO-----------  | ___ \ |/ _\ |  _/ _ \ / _ \| __|'
echo '----(---)----------  | |_/ / | (_| | || (_) | (_) | |_ '
echo '-----\--(----------  \____/|_|\__, |_| \___/ \___/ \__|'
echo '------\_)----------            __/ |'
echo '-----------Oooo----           |___/'
echo '----------(----)---  ______         _ '
echo '-----------)--/----  |  ___|       | |'
echo '-----------(_/-----  | |_ __ _  ___| |_ ___'
echo '----oooO-----------  |  _/ _` |/ __| __/ __|'
echo '----(---)----------  | || (_| | (__| |_\__ \'
echo '-----\--(----------  \_| \__,_|\___|\__|___/'
echo '------\_)----------'
echo
echo ${FACT}
echo

if [ $PLATFORM = "Darwin" ]
then
  afplay .fact.mp3 &
fi
