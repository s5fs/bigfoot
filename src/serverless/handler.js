'use strict';
const facts = require('./facts.json')

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getFact() {
  return facts[getRandomInt(0, facts.length)]
}

module.exports.handle = (event, context, callback) => {
  const response = {
    statusCode: 200,
    body: getFact()
  };

  callback(null, response);
};
