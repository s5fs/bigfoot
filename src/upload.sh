#!/bin/bash

while read fact; do
  TEXT=`echo ${fact} | sed -e 's/\(\,\)*$//g' | sed 's/[][]//g'`
  NBR=`echo ${TEXT} | sed -e 's/.*#\(.*\):.*/\1/'`
  FILENAME=fact_${NBR}.mp3

  aws polly synthesize-speech \
    --output-format mp3 \
    --voice-id Justin \
    --text "${TEXT}" \
    mp3/${FILENAME}

aws s3 cp mp3/${FILENAME} s3://bigfoot-is-real/mp3/${FILENAME} --acl public-read
done <serverless/facts.json

