#!/bin/bash

echo Deploying shell script
aws s3 cp src/fact.sh s3://bigfoot-is-real/facts --acl public-read

echo Deploying serverless app
cd src/serverless
serverless deploy
cd ../../
